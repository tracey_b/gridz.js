** What is Gridz.js **

Gridz.js takes the divs from a single HTML file and turns each div into it's own grid page( think Dungeon Master ) that can be navigated via keyboard, buttons, or Jquery Mobile swipe gestures. It is PhoneGap friendly and has been implemented on both iOS and Android.

Check out the demo at gridz.optimalprime.com

** Getting Started **

Getting started with Gridz is easy. In your index file simple create divs that represent each "Cell" on your grid. Then, set the div id to the coordinates using the format
c[X Coordinate]x[Y Coordinate] .
See code example below.

Example div Coordinates

<div id="c0x0"></div>
<div id="c0x-1"></div>

The JS will do the rest. In order to add button navigation add the following divs to a class.

Example Navigation

<div id="c0x0">
<div class="goup">Up</div>
<div class="godown">Down</div>
<div class="goleft">Left</div>
<div class="goright">Right</div>
</div>

** JS Settings ** 

By default overlay map( currently this function is in beta ), keyboard navigation, and swipe navigation are enabled. These can be disabled by changing their respective variables to "false" in the gridz.js file.

** Notes ** 

Grids.js is still in beta. Feel free to improve it! If you build something awesome or have questions/suggestions let me know tracey.boyd[at]kotebo[.]com

** License ** 

GPLv3