// Global Variables

var mapUp = null;

/*
*
*   Settings - Feel free to change things here.
*
*/

  // Allow Jquery Mobile Swipe
  var allowSwipe = false;

  // Allows keyboard arrow control.
  var allowKeyboardControl = true;

  // Enables Overlay Map
  var overlayMap = true;

  // Enables console logging.
  var debugMode = false;


/*
*
*   End Settings - Use caution changing anything below here.
*
*/

// Prevents navigation while map is up.


// Add swipe up/down which are not supported natively by jQuery mobile
(function() {
    var supportTouch = $.support.touch,
            scrollEvent = "touchmove scroll",
            touchStartEvent = supportTouch ? "touchstart" : "mousedown",
            touchStopEvent = supportTouch ? "touchend" : "mouseup",
            touchMoveEvent = supportTouch ? "touchmove" : "mousemove";
    $.event.special.swipeupdown = {
        setup: function() {
            var thisObject = this;
            var $this = $(thisObject);
            $this.bind(touchStartEvent, function(event) {
                var data = event.originalEvent.touches ?
                        event.originalEvent.touches[ 0 ] :
                        event,
                        start = {
                            time: (new Date).getTime(),
                            coords: [ data.pageX, data.pageY ],
                            origin: $(event.target)
                        },
                        stop;

                function moveHandler(event) {
                    if (!start) {
                        return;
                    }
                    var data = event.originalEvent.touches ?
                            event.originalEvent.touches[ 0 ] :
                            event;
                    stop = {
                        time: (new Date).getTime(),
                        coords: [ data.pageX, data.pageY ]
                    };

                    // prevent scrolling
                    if (Math.abs(start.coords[1] - stop.coords[1]) > 10) {
                        event.preventDefault();
                    }
                }
                $this
                        .bind(touchMoveEvent, moveHandler)
                        .one(touchStopEvent, function(event) {
                    $this.unbind(touchMoveEvent, moveHandler);
                    if (start && stop) {
                        if (stop.time - start.time < 1000 &&
                                Math.abs(start.coords[1] - stop.coords[1]) > 30 &&
                                Math.abs(start.coords[0] - stop.coords[0]) < 75) {
                            start.origin
                                    .trigger("swipeupdown")
                                    .trigger(start.coords[1] > stop.coords[1] ? "swipeup" : "swipedown");
                        }
                    }
                    start = stop = undefined;
                });
            });
        }
    };
    $.each({
        swipedown: "swipeupdown",
        swipeup: "swipeupdown"
    }, function(event, sourceEvent){
        $.event.special[event] = {
            setup: function(){
                $(this).bind(sourceEvent, $.noop);
            }
        };
    });

})();

// Maps current location of relevant divs based on current origin.
function divMap () {

	// Removes class on divs
	if ($(".left")[0]){
	$(leftdiv).removeClass( "left" ); }
	if ($(".right")[0]){
	$(rightdiv).removeClass( "right" ); }
	if ($(".bottom")[0]){
	$(bottomdiv).removeClass( "bottom" ); }
	if ($(".top")[0]){
	$(topdiv).removeClass( "top" ); }
	if ($(".center")[0]){
	$(center).removeClass( "center" ); }

  if ( overlayMap == true ) {

  	// Removes class from map divs
  	if ($(".leftmap")[0]){
  	$(leftdivmap).removeClass( "leftmap" ); }
  	if ($(".rightmap")[0]){
  	$(rightdivmap).removeClass( "rightmap" ); }
  	if ($(".bottommap")[0]){
  	$(bottomdivmap).removeClass( "bottommap" ); }
  	if ($(".topmap")[0]){
  	$(topdivmap).removeClass( "topmap" ); }
  	if ($(".centermap")[0]){
  	$(centermap).removeClass( "centermap" ); }

  }

	// Determines div's based on current origin.
	center = "#c" + origin[0] + "x" + origin[1];
	leftdiv = "#c" + ( parseInt(origin[0]) - 1 ) + "x" + origin[1];
	rightdiv = "#c" + ( parseInt(origin[0]) + 1 ) + "x" + origin[1];
	bottomdiv = "#c" + origin[0] + "x" + ( parseInt(origin[1]) - 1 );
	topdiv = "#c" + origin[0] + "x" + ( parseInt(origin[1]) + 1 );

  if ( overlayMap == true ) {

  	// Determines map div's based on current origin.
  	centermap = "#c" + origin[0] + "x" + origin[1] + "map";
  	leftdivmap = "#c" + ( parseInt(origin[0]) - 1 ) + "x" + origin[1] + "map";
  	rightdivmap = "#c" + ( parseInt(origin[0]) + 1 ) + "x" + origin[1] + "map";
  	bottomdivmap = "#c" + origin[0] + "x" + ( parseInt(origin[1]) - 1 ) + "map";
  	topdivmap = "#c" + origin[0] + "x" + ( parseInt(origin[1]) + 1 ) + "map";

  }

	// Re-adds classes to divs
	$(center).addClass( "center" );
	$(topdiv).addClass( "top" );
	$(leftdiv).addClass( "left" );
	$(rightdiv).addClass( "right" );
	$(bottomdiv).addClass( "bottom" );

  if ( overlayMap == true ) {

  	// Re-adds classes to map divs
  	$(centermap).addClass( "centermap" );
  	$(topdivmap).addClass( "topmap" );
  	$(leftdivmap).addClass( "leftmap" );
  	$(rightdivmap).addClass( "rightmap" );
  	$(bottomdivmap).addClass( "bottommap" );

  	// Remaps styles to the map highlighting where the user is.
  	mapStyles ()

  }

  if ( debugMode == true ) {

  console.log( "end" );
	console.log( "center " + center );
	console.log( "leftdiv " + leftdiv );
	console.log( "rightdiv" + rightdiv );
	console.log( "bottomdiv" + bottomdiv );
	console.log( "topdiv" + topdiv );
	console.log('break');

  }

}

function removeStyles() {

	// Remove inline style from divs
	$("div.center").removeAttr("style")
	$("div.top").removeAttr("style")
	$("div.bottom").removeAttr("style")
	$("div.left").removeAttr("style")
	$("div.right").removeAttr("style")

  if ( overlayMap == true ) {

    // Remove inline style from map divs
  	$("div.centermap").removeAttr("style")
  	$("div.topmap").removeAttr("style")
  	$("div.bottommap").removeAttr("style")
  	$("div.leftmap").removeAttr("style")
  	$("div.rightmap").removeAttr("style")

  }

}

function bindEvents() {

  if ( allowSwipe == true ) {

	$('div.center').on("swipeup", function( event ){ event.stopImmediatePropagation(); swipeUpEvent() });
	$('div.center').on("swipedown", function( event ){ event.stopImmediatePropagation(); swipeDownEvent ()});
	$('div.center').on("swipeleft", function( event ){ event.stopImmediatePropagation(); swipeLeftEvent () });
	$('div.center').on("swiperight", function( event ){ event.stopImmediatePropagation(); swipeRightEvent ()});

  }

  if ( allowKeyboardControl == true ) {
	// Binds Arrow Keys
  	$(document).keydown(function(event){

  		event.stopImmediatePropagation();

  		if (event.keyCode == 40 ) {
  	 		swipeUpEvent()
  		}
  		if (event.keyCode == 38 ) {
  	 		swipeDownEvent();
  		}
  		if (event.keyCode == 39 ) {
  	 		swipeLeftEvent();
  		}
  		if (event.keyCode == 37 ) {
  	 		swipeRightEvent();
  		}

  		$(document).unbind("keydown");

      });

  }

	//Div tap binding

	$('div.goleft').on("tap", function( event ){ event.stopImmediatePropagation(); swipeRightEvent() });
	$('div.goright').on("tap", function( event ){ event.stopImmediatePropagation(); swipeLeftEvent() });
	$('div.goup').on("tap", function( event ){ event.stopImmediatePropagation(); swipeDownEvent() });
	$('div.godown').on("tap", function( event ){ event.stopImmediatePropagation(); swipeUpEvent() });

	//Div click binding

	$('div.goleft').on("click", function( event ){ event.stopImmediatePropagation(); swipeRightEvent() });
	$('div.goright').on("click", function( event ){ event.stopImmediatePropagation(); swipeLeftEvent() });
	$('div.goup').on("click", function( event ){ event.stopImmediatePropagation(); swipeDownEvent() });
	$('div.godown').on("click", function( event ){ event.stopImmediatePropagation(); swipeUpEvent() });


  //	console.log( "events bound" );
}



function unBindEvents() {

	$('div.center').unbind("swipeup");
	$('div.center').unbind("swipedown");
	$('div.center').unbind("swipeleft");
	$('div.center').unbind("swiperight");
	$(document).unbind("keydown");

}


// Used to fade out elements.
function elementFadeOut () {

	$('div.goleft').animate({ 'opacity':'0' },function(){});
	$('div.goright').animate({ 'opacity':'0' },function(){});
	$('div.goup').animate({ 'opacity':'0' },function(){});
	$('div.godown').animate({ 'opacity':'0' },function(){});

}

// Used to fade in elements.
function elementFadeIn () {

	$('div.goleft').animate({ 'opacity':'1' },function(){});
	$('div.goright').animate({ 'opacity':'1' },function(){});
	$('div.goup').animate({ 'opacity':'1' },function(){});
	$('div.godown').animate({ 'opacity':'1' },function(){});

}


function mapFadeInOut () {

  if ( overlayMap == true ) {

    if ( mapUp === null ) {
  		$('#map').css('display','inline').css('opacity','0').animate({ 'opacity':'1' },function(){
  		mapUp = setTimeout(function() { $('#map').animate({ 'opacity':'0' },function(){ $("#map").removeAttr("style"); mapUp = null; }); }, 600);
  		});
  	} else {
  	clearTimeout(mapUp);
  	mapUp = null;
  	mapUp = setTimeout(function() { $('#map').animate({ 'opacity':'0' },function(){ $("#map").removeAttr("style"); mapUp = null; }); }, 600);
  	}

  }

}


function mapFadeIn () {

  if ( overlayMap == true ) {

    if ( mapUp === null ) {
  		$('#map').css('display','inline').css('opacity','0').animate({ 'opacity':'1' },function(){ });
  	}

  }

}

function mapFadeOut () {

  if ( overlayMap == true ) {

  	if ( mapUp === null ) {
  	mapUp = setTimeout(function() { $('#map').animate({ 'opacity':'0' },function(){ $("#map").removeAttr("style"); mapUp = null; }); }, 600);
  	} else {
  	clearTimeout(mapUp);
  	mapUp = null;
  	mapUp = setTimeout(function() { $('#map').animate({ 'opacity':'0' },function(){ $("#map").removeAttr("style"); mapUp = null; }); }, 600);
  	}

  }

}

function mapStyles () {

  if ( overlayMap == true ) {

  	$("div.centermap").css('border-color','red');
  	$("div.topmap").css('border-color','green');
  	$("div.bottommap").css('border-color','green');
  	$("div.leftmap").css('border-color','green');
  	$("div.rightmap").css('border-color','green');

  }

}

function swipeDownEvent () {
//	console.log( "swipe down" );
	//Bump if topped reached

//	unBindEvents()
	if ($(topdiv).length == 0) {

	$('div.center').animate({ 'top':'5%' }, 100 ,function(){ $('div.center').animate({ 'top':'0%'}, 100, function(){
	removeStyles()
	bindEvents ()
	})});


	} else {


		var topdown = $('div.top').css('bottom','100%').css('display','inline').animate({ 'bottom':'0' },function(){   });
		var centerdown = $('div.center').animate({ 'top':'100%' },function(){  });

		$.when(topdown, centerdown, mapFadeInOut() ).done(function() {
			removeStyles()

      if ( debugMode == true ) { console.log( "down animations complete" ); }

			origin[1] = ( parseInt(origin[1]) + 1 );
			divMap ()

      if ( debugMode == true ) { console.log( origin[1] ); }

      bindEvents ()
		});
	}
}

function swipeUpEvent () {

  if ( debugMode == true ) { console.log( "swipe up" ); }

  //Bump if bottom reached
  //	unBindEvents()

	if ($(bottomdiv).length == 0) {

	$('div.center').animate({ 'bottom':'5%' }, 100 ,function(){ $('div.center').animate({ 'bottom':'0%'}, 100, function(){
	removeStyles()
	bindEvents ()
	})});


	} else {

		var bottomup = $('div.bottom').css('top','100%').css('display','inline').animate({ 'top':'0' },function(){ });
		var centerup = $('div.center').animate({ 'bottom':'100%' },function(){ });

		$.when(bottomup, centerup, mapFadeInOut() ).done(function() {
			removeStyles()

      if ( debugMode == true ) { console.log( "up animations complete" ); }

			origin[1] = ( parseInt(origin[1]) - 1 );
			divMap ()

      if ( debugMode == true ) { console.log( origin[1] ); }

			bindEvents ()
		});
	}

}


function swipeLeftEvent () {

  if ( debugMode == true ) { console.log( "swipe left" );}

	// For some reason left and right events need to be unbound or else they will bubble.
  //	$('div.center').unbind("swipeleft");

	//Bump if right reached
	if ($(rightdiv).length == 0) {

	$('div.center').animate({ 'right':'5%' }, 100 ,function(){ $('div.center').animate({ 'right':'0%'}, 100, function(){
	removeStyles()
	bindEvents ()
	})});


	} else {


		var rightleft = $('div.right').css('left','100%').css('display','inline').animate({ 'left':'0%' },function(){ });
		var centerleft = $('div.center').animate({ 'right':'100%' },function(){ });

		$.when(rightleft, centerleft, mapFadeInOut() ).done(function() {
			removeStyles()

    if ( debugMode == true ) { console.log( "left animations complete" );}

			origin[0] = ( parseInt(origin[0]) + 1 );
			divMap ()

    if ( debugMode == true ) { console.log( origin[0] ); }

			bindEvents ()
		});
	}


}

function swipeRightEvent () {

  if ( debugMode == true ) { console.log( "swipe right" ); }


	// For some reason left and right events need to be unbound or else they will bubble.
  //	$('div.center').unbind("swiperight");

	//Bump if left reached
	if ($(leftdiv).length == 0) {

	$('div.center').animate({ 'left':'5%' }, 100 ,function(){ $('div.center').animate({ 'left':'0%'}, 100, function(){
	removeStyles()
	bindEvents ()
	})});


	} else {

		var leftright = $('div.left').css('right','100%').css('display','inline').animate({ 'right':'0%' },function(){ });
		var centerright = $('div.center').animate({ 'left':'100%' },function(){ });

		$.when(leftright, centerright, mapFadeInOut() ).done(function() {
			removeStyles()

      if ( debugMode == true ) { console.log( "right animations complete" ); }
//
			origin[0] = ( parseInt(origin[0]) - 1 );
			divMap ()

		  if ( debugMode == true ) { console.log( origin[0] ); }

      bindEvents ()

		});
	}


}



$( document ).ready(function() {


	// Starting Coordinates( different then the above constant )
	window.origin = new Array();
		window.origin[0] = "0";
		window.origin[1] = "0";


	divMap ()

  if ( debugMode == true ) {

    console.log(origin);
  	console.log(center);
  	console.log(leftdiv);
  	console.log(rightdiv);
  	console.log(bottomdiv);
  	console.log(topdiv);

  }

	// Initial Event Binds
	bindEvents ()

});
